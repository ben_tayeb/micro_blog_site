var sql = require("./db/connectdb.js");
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var path = require('path');
var app = express();

var dateTime = require('node-datetime');


app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

//integre css
app.use(express.static(__dirname+'/public'));

app.get('/' , function(request, response) {
	if (!request.session.loggedin) {
		response.sendFile(path.join(__dirname + '/public/html/index.html'));		
	}
})

app.get('/index.html' , function(request, response) {
	if (!request.session.loggedin) {
		response.sendFile(path.join(__dirname + '/public/html/index.html'));		
	}
})


app.post('/inscription', function(request, response) {
	var nom = request.body.nom;
	var prenom = request.body.prenom;
	var email = request.body.email_i;
	var mdp = request.body.mdp;


	console.log(nom,prenom,email,mdp)
	if (nom!=''&&prenom!=''&&email!=''&&mdp!='') {
		var checkEmail=true;

		sql.con.query('SELECT * FROM user WHERE email="'+email+'"',
		function(error,results) {
			
			console.log('?')
			console.log(email)
			console.log(results.length)
			if(error){
				console.log(console.log(error))
			}
			if (results.length>0) {
				checkEmail=false;
			}
			console.log(checkEmail)
			if(checkEmail==true){
				sql.con.query('INSERT INTO user (nom,prenom,email,motdepasse) VALUES ("'+nom+'","'+prenom+'","'+email+'","'+mdp+'")',
				function(error) {
					
					if (error) {
						console.log("noo");
						console.log(error);
					}
		
				});
					response.send("Vous vous êtes inscrit ! ")
					response.end();
		
			}else{
				response.send("mailExistant");
			}
		});
	}else{
		response.send(null)
	}
	
});


app.post('/connexion', function(request, response) {
	var mail = request.body.email;
	var mdp = request.body.mdp;
	console.log(mail,mdp)
	if (mail && mdp) {
		sql.con.query('SELECT * FROM user WHERE email = ? AND motdepasse = ?',
		 				[mail, mdp], function(error,results) {
			console.log("?");

			if (error) {
				console.log(error,"noo");
			}
			console.log(results)
			if (results.length>0) {
				console.log(results[0].id)
				request.session.loggedin = true;
				request.session.email = mail;
				request.session.user_id = results[0].id;
				
				response.redirect(('/pageUser'));
			} else {
				response.send(null);
			}			
		});
	} else {
		response.send(null);
		response.end();
	}
});	

app.get('/deconnexion' , function(request, response) {
	if(request.session.loggedin){
		request.session.loggedin = false;
		response.redirect('/index.html')
	}else{
		response.send(null)

	}
})

app.get('/pageUser' , function(request, response) {
	if(request.session.loggedin){
		response.sendFile(path.join(__dirname + '/public/html/pageUser.html'));		
	}else{
		response.send("Veuillez vous connectez avant d'acceder a cette page")
	}
})

app.post('/monProfil',function(request,response){

	if (request.session.loggedin) {
		sql.con.query('SELECT * FROM user WHERE id='+request.session.user_id,
		 function(error,results) {

			console.log('prof')

			if (results) {
				response.send(results)
			}
		});
	}

})

app.post('/modifierUser' , function(request, response) {
	console.log("modif")
	var nom = request.body.nom;
	var prenom = request.body.prenom;
	var email = request.body.email;
	var mdp = request.body.mdp;

	if (request.session.loggedin) {
		sql.con.query('UPDATE user SET nom="'+nom+'",prenom="'+prenom+'",email="'+email+'",motdepasse="'+mdp+'" WHERE id='+request.session.user_id ,
		function(error,results) {
		
			if (error) {
				console.log("noo");
				console.log(error);
				response.send(null)

			}
			console.log(results)
			response.send(true)
		});	
	}else{
		response.send(null)
	}

})

app.post('/poster_publication' , function(request, response) {
	const comm = request.body.commentaire
	var user = request.body.user
	if (user=='') {
		user="@everyone"
	}
	console.log("user",user)
	var date = dateTime.create();
	var dateformat = date.format('à HhM le d/m/Y');
	if(request.session.loggedin && comm){
		console.log(dateformat)
		sql.con.query('INSERT INTO publication (id_user,commentaires,likes,dislikes,date_publication,pourQui) VALUES ('+request.session.user_id+',"'+comm+'",0,0,"'+dateformat+'","'+user+'")',
		function(error) {
			
			if (error) {
				console.log("noo");
				console.log(error);
			}

		});
		response.send("Nouvelle publication")
	}else{
		response.send(null)
	}
})

app.post('/affichePublication' , function(request, response) {
	const typePubli = request.body.type

	if (typePubli=="Les publications qui me concernent") {
		sql.con.query('SELECT * FROM user JOIN publication ON user.id=publication.id_user WHERE publication.pourQui="'+request.session.email+'" ORDER by publication.id DESC ',
		function(error,results) {
			
		if (error) {
			console.log("noo");
			console.log(error);
		}
		response.send(results)
		});
	}else if(typePubli=="Les publications populaires"){
		sql.con.query('SELECT * FROM user JOIN publication ON user.id=publication.id_user WHERE publication.pourQui="@everyone" ORDER by publication.likes DESC',
			function(error,results) {
				
			if (error) {
				console.log("noo");
				console.log(error);
			}
			response.send(results)
		});
	}else if(typePubli=="Mes publications"){
		sql.con.query('SELECT * FROM user JOIN publication ON user.id=publication.id_user WHERE id_user='+request.session.user_id+' ORDER by publication.id DESC',
			function(error,results) {
				
			if (error) {
				console.log("noo");
				console.log(error);
			}
			response.send(results)
		});
	}else if(typePubli=="Mes favoris"){
		sql.con.query('SELECT * FROM favoris JOIN publication ON favoris.id_publication=publication.id JOIN user ON publication.id_user=user.id WHERE favoris.id_user='+request.session.user_id+' ORDER by publication.id DESC',
			function(error,results) {
				
			if (error) {
				console.log("noo");
				console.log(error);
			}
			response.send(results)
		});
	}else if(typePubli=="Les publications que j'aime"){
		sql.con.query('SELECT * FROM liker JOIN publication ON liker.id_publication=publication.id JOIN user ON publication.id_user=user.id WHERE liker.id_user='+request.session.user_id+' ORDER by publication.id DESC',
			function(error,results) {
				
			if (error) {
				console.log("noo");
				console.log(error);
			}
			response.send(results)
		});
	}else{
		sql.con.query('SELECT * FROM user JOIN publication ON user.id=publication.id_user WHERE publication.pourQui="@everyone" ORDER by publication.id DESC',
			function(error,results) {
				
			if (error) {
				console.log("noo");
				console.log(error);
			}
			response.send(results)
		});
	}
})

app.post('/like' , function(request, response) {
	console.log("like")
	var likes = request.body.nblikes;
	var id = request.body.id_publi;
	
	sql.con.query('SELECT * FROM liker WHERE id_publication='+id+' AND id_user='+request.session.user_id ,
		function(error,results) {
		
		if (error) {
			console.log("noo");
			console.log(error);
		}
		if (!results.length>0) {
			sql.con.query('INSERT INTO liker (id_user,id_publication) VALUES ('+request.session.user_id+','+id+')',
			function(error) {
				
				if (error) {
					console.log("noo");
					console.log(error);
				}
			});

			sql.con.query('UPDATE publication SET likes=('+(parseInt(likes)+1)+') WHERE id='+id ,
				function(error,results) {
				
				if (error) {
					console.log("noo");
					console.log(error);
				}
				response.send(true)
			});
		}else{
			sql.con.query('DELETE FROM liker WHERE id_user='+request.session.user_id+' AND id_publication='+id ,
				function(error,results) {
				
				if (error) {
					console.log("noo");
					console.log(error);
				}
			});
			sql.con.query('UPDATE publication SET likes=('+(parseInt(likes)-1)+') WHERE id='+id ,
			function(error,results) {
			
			if (error) {
				console.log("noo");
				console.log(error);
			}
			response.send(false)
		});
		}
	});
	
})

app.post('/dislike' , function(request, response) {
	console.log("dislike")
	var dislikes = request.body.nbdislikes;
	var id = request.body.id_publi;
	
	sql.con.query('SELECT * FROM disliker WHERE id_publication='+id+' AND id_user='+request.session.user_id ,
		function(error,results) {
		
		if (error) {
			console.log("noo");
			console.log(error);
		}
		if (!results.length>0) {
			sql.con.query('INSERT INTO disliker (id_user,id_publication) VALUES ('+request.session.user_id+','+id+')',
			function(error) {
				
				if (error) {
					console.log("noo");
					console.log(error);
				}
			});

			sql.con.query('UPDATE publication SET dislikes=('+(parseInt(dislikes)-1)+') WHERE id='+id ,
				function(error,results) {
				
				if (error) {
					console.log("noo");
					console.log(error);
				}
				response.send(true)
			});
		}else{
			sql.con.query('DELETE FROM disliker WHERE id_user='+request.session.user_id+' AND id_publication='+id ,
				function(error,results) {
				
				if (error) {
					console.log("noo");
					console.log(error);
				}
			});
			sql.con.query('UPDATE publication SET dislikes=('+(parseInt(dislikes)+1)+') WHERE id='+id ,
			function(error,results) {
			
			if (error) {
				console.log("noo");
				console.log(error);
			}
			response.send(false)
		});
		}
	});
})

app.post('/favoris' , function(request, response) {
	console.log("fav")
	var id = request.body.id_publi;
	sql.con.query('SELECT * FROM favoris WHERE id_user='+request.session.user_id+' AND id_publication='+id ,
		function(error,results) {
		
		if (error) {
			console.log("noo");
			console.log(error);
		}
		console.log(results)
		if (!results.length > 0 ) {
			sql.con.query('INSERT INTO favoris (id_user,id_publication) VALUES ('+request.session.user_id+','+id+')' ,
				function(error,results) {
				
				if (error) {
					console.log("noo");
					console.log(error);
				}
				console.log(results)
				response.send(true)
			});
		}else{
			sql.con.query('DELETE FROM favoris WHERE id_user='+request.session.user_id+' AND id_publication='+id ,
				function(error,results) {
				
				if (error) {
					console.log("noo");
					console.log(error);
				}
				console.log(results)
				response.send(false)
			});
		}
	});
})

app.post('/listeUsers' , function(request, response) {
	
	sql.con.query('SELECT email FROM user WHERE id!='+request.session.user_id,
		function(error,results) {
		
		if (error) {
			console.log("noo");
			console.log(error);
		}
		response.send(results)
	});
})


app.listen(8001);