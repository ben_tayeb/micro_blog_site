
var mysql = require('mysql');

var con = mysql.createConnection({
  host: "private.localhost",
  user: "jaber",
  password: "",
  database: "jardin_des_commentaires"
});


con.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }

  console.log('connected as id ' + con.threadId);
});
  
module.exports = {
	con,
	mysql
}