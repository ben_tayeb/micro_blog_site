
$(document).ready (function () {
    $("#publications").on('click','.dislike',function(){
        var id = $(this).closest('.annonce').attr('id')
        var input = $(this).closest('.div').find(".nbdislikes")
        var dislikes = $(this).closest('.div').find(".nbdislikes").val();


        $.ajax({
            url: '/dislike',
            type: 'POST',
            data: {
                id_publi : id,
                nbdislikes : dislikes,
            },
            success: function(data){
                console.log(data)
                if(data){
                    input.val(parseInt(dislikes)-1)
                }
                else{
                    input.val(parseInt(dislikes)+1)
                }
            },
            error: function(){ 
                console.log("le dislike a échouée")
            }
        });
    });
});