$(document).ready (function () {

    affichePublication();

    setInterval(affichePublication,10000)


    function affichePublication() {
        $.ajax({
            url: '/affichePublication',
            type: 'POST',
            data: {
                type:$('#typePubli').val()
            },
            success: function(data){
                if(data){

                    $('#publications').empty()
                                            
                        for (let index = 0; index < data.length; index++) {
                            const publication = data[index];

                            const annonce = document.createElement('div');
                            annonce.className='annonce'
                            if (publication.id_publication) {
                                annonce.setAttribute('id',publication.id_publication)
                            }else{
                                annonce.setAttribute('id',publication.id) 
                            }

                            const annonceHeader = document.createElement('div');
                            annonceHeader.className="annonceHeader"
                            
                            const annonceTitre = document.createElement('h2');
                            annonceTitre.className='annonceTitre'
                            annonceTitre.append(publication.nom +" "+publication.prenom)

                            const img = document.createElement('img')

                            const div = document.createElement('div');
                            div.className='div'

                            const divtexte = document.createElement('div')
                            divtexte.className='annonceTexte'

                            const p = document.createElement('p')
                            p.append(publication.commentaires)
                            p.setAttribute('style',"border: 1px solid;")

                            
                            const buttonlike = document.createElement('button')
                            buttonlike.setAttribute('class',"like")
                            buttonlike.append("Like")

                            const buttondisLike = document.createElement('button')
                            buttondisLike.setAttribute('class',"dislike")
                            buttondisLike.append("DisLike")

                            const buttonfavoris = document.createElement('button')
                            buttonfavoris.setAttribute('class',"favoris")
                            buttonfavoris.append("favoris")

                            const inputLike = document.createElement('input');
                            inputLike.setAttribute('class',"nblikes")
                            inputLike.setAttribute('type',"button")
                            inputLike.setAttribute('style',"width: 50px;")
                            inputLike.setAttribute('value',publication.likes)
                            inputLike.setAttribute('readOnly',"true")

                            const inputdisLike = document.createElement('input');
                            inputdisLike.setAttribute('class',"nbdislikes")
                            inputdisLike.setAttribute('type',"number")
                            inputdisLike.setAttribute('style',"width: 50px;")
                            inputdisLike.setAttribute('value',publication.dislikes)
                            inputdisLike.setAttribute('readOnly',"true")


                            const h3send = document.createElement('h4')
                            h3send.append(publication.email+" send to "+publication.pourQui+" "+publication.date_publication)

                            document.getElementById("publications").appendChild(annonce);
                            
                            annonce.appendChild(annonceHeader)
                            annonce.appendChild(div)

                            annonceHeader.appendChild(annonceTitre)
                            annonceHeader.appendChild(h3send)

                            div.appendChild(divtexte)
                            divtexte.appendChild(p)

                            div.appendChild(buttonlike)
                            div.appendChild(inputLike)
                            div.appendChild(buttondisLike)
                            div.appendChild(inputdisLike)
                            div.appendChild(buttonfavoris)

                        }
                    
                }else{
                    console.log("Pas de publi dispo :(");
                }
            },
            error: function(){ 
                console.log("la maj des publications a échouée")
            }
        });
    }

});
