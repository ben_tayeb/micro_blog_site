$(document).ready (function () {
	
	$("#modifier").click(function(){
            
        $.ajax({
            url: '/modifierUser',
            type: 'POST',

            data : {
                nom : $("#nom").val(),
                prenom : $("#prenom").val(),
                email :$("#email").val(),
                mdp: $("#mdp").val()
            },

            success: function(data){
                alert("votre compte a été modifié")
                window.location.href = "http://localhost:8001/pageUser"
            },
            error: function(){ 
                console.log("la modification a échouée")
            }
        });
    });		
});