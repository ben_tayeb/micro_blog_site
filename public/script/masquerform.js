$(document).ready (function () {
	
	$("#inscription").click(function(){
		if ( $( ".inscription" ).first().is( ":hidden" ) ) {
            $( ".inscription" ).slideDown( "slow" );
            $( "#publications" ).hide("slow");
            $( ".connexion" ).hide("slow");
            $( "#connexion" ).hide("slow");
    		$("#inscription").text("Retour");
		} else {
            $( ".inscription" ).hide("slow");
            $( "#publications" ).slideDown("slow");
            $( "#connexion" ).slideDown("slow");
            $("#inscription").text("Inscription");
            
        }
	});
        
    $("#connexion").click(function(){
		if ( $( ".connexion" ).first().is( ":hidden" ) ) {
            $( ".connexion" ).slideDown( "slow" );
            $( "#publications" ).hide("slow");
            $( ".inscription" ).hide("slow");
            $( "#inscription" ).hide("slow");
    		$("#connexion").text("Retour");
		} else {
            $( ".connexion" ).hide("slow");
            $( "#publications" ).slideDown("slow");
            $( "#inscription" ).slideDown("slow");
    		$("#connexion").text("Se connecter");
        }
	});

});