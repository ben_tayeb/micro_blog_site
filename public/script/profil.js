$(document).ready (function () {
	
	$("#monProfil").click(function(){


		if ( $( ".profil" ).first().is( ":hidden" ) ) {
            
            $.ajax({
                url: '/monProfil',
                type: 'POST',
    
                success: function(data){
                    $( ".profil" ).slideDown( "slow" );
                    $( "#publications" ).hide("slow");
                    $( "#type" ).hide("slow");
                    $( "#text" ).hide("slow");
                    $( "#deconnexion" ).hide("slow");
                    $("#monProfil").text("Retour");
                    $("#nom").val(data[0].nom)
                    $("#prenom").val(data[0].prenom)
                    $("#email").val(data[0].email)
                    $("#mdp").val(data[0].motdepasse)

                },
                error: function(){ 
                    console.log("la maj profil a échouée")
                }
            });

		} else {
            $( ".profil" ).hide("slow");
            $( "#publications" ).slideDown("slow");
            $( "#type" ).slideDown("slow");
            $( "#text" ).slideDown("slow");
            $( "#deconnexion" ).slideDown("slow");
            $("#monProfil").text("Mon Profil");
            
        }
    });
});