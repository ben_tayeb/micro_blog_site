$(document).ready (function () {
	
    $("#inscrire").click(function() {
        console.log("test")
        
        $.ajax({
            url: '/inscription',
            type: 'POST',
            data: {
                nom : $("#nom").val(),
                prenom : $("#prenom").val(),
                email_i : $("#email_i").val(),
                mdp : $("#mdp_i").val()
            },
            success: function(data){
                if (data=="mailExistant") {
                    alert("l'Email ou l'Username est déjà utilisé, Veuillez recommencer l'inscription !")
                }
                else if(data){
                    alert(data)
                    $("#inscription").click();
                }else{
                    alert("Veuillez remplir tout les champs !");
                }
            },
            error: function(){ 
                console.log("l'inscription a échouée")
            }
        });
        $("#nom").val('')
        $("#prenom").val('')
        $("#email_i").val('')
        $("#mdp_i").val('')
    });

});