drop table if exists user;
drop table if exists publication;
drop table if exists favoris;
drop table if exists liker;
drop table if exists disliker;

create table user ( id INT primary key AUTO_INCREMENT, 
                    nom VARCHAR(250),
                    prenom VARCHAR(250), 
                    email VARCHAR(250),
                    motdepasse VARCHAR(250));

create table publication (  id INT primary key AUTO_INCREMENT, 
                            id_user INT, 
                            commentaires VARCHAR(250), 
                            likes INT,
                            dislikes INT,
                            date_publication VARCHAR(250),
                            pourQui VARCHAR(250)
                        );

create table favoris (  id INT primary key AUTO_INCREMENT, 
                        id_user INT, 
                        id_publication INT 
                        );
                    
create table liker (  id INT primary key AUTO_INCREMENT, 
                        id_user INT, 
                        id_publication INT 
                        );

create table disliker (  id INT primary key AUTO_INCREMENT, 
                        id_user INT, 
                        id_publication INT 
                        );
-- remplissage des tables

INSERT INTO user VALUES 
(1,"toto","tata",'toto@live.fr','okok'),
(2,"titi","tonton",'titi@live.fr','ok'),
(3,"tutu","Jean",'tutu@live.fr','ok');


INSERT INTO publication VALUES 
(1,1,"Bonjour Professeurs, Quel année épuisante !",0,-4,"à 12h15 le 12/05/2021","@everyone"),
(2,1,"C'est les vacances !",45,0,"à 12h45 le 12/05/2021","@everyone"),
(3,2,"Hier gros match !",0,0,"à 13h15 le 12/05/2021","toto@live.fr"),
(4,2,"Je vends une voiture contactez-moi si interressé",0,-1,"à 13h15 le 13/05/2021","@everyone"),
(5,2,"J'ai besoin d'aide pour mon devoir si ya quelqun ?????",4,-3,"à 17h15 le 14/05/2021","@everyone"),
(6,1,"Je recherhe une personne serieuse pour m'aider à bricoler, envoyer-moi votre numero en privé",1,0,"à 19h15 le 15/05/2021","@everyone"),
(7,3,"Quelqu'un peut me conseillez un bon medecin sur Paris ?",3,0,"à 20h15 le 15/05/2021","@everyone");


 
